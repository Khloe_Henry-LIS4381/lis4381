<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Khloe Henry">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment1</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style>
			body {
				background-color: #f4e8eb;
			}
		</style>


  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-center">
					<strong>Requirements:</strong>Petstore ERD; Concert Tickets App;; Skillset 4 (Decison Structures); Skillset 5 (Nested Structures); Skillset 6 (Methods)
					</p>
			<div class="container">
				<h4>Concert Tickets App</h4>
				<img src="img/Concert_Ticket_A.jpg" class="img-responsive center-block" style="width: 250px; float: left; margin-left: 300px;" alt="JDK Installation">
				<img src="img/Concert_Ticket_B.jpg" class="img-responsive center-block" style="width: 250px; float: left; margin-left:20px;" alt="Android Studio Installation">
				</div>
			<div class="container">
				<h4>Petstore ERD</h4>
				<img src="img/petstore_erd.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
			<div class="container">
				<h4>Customer Table</h4>
				<img src="img/customer.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
			<div class="container">
				<h4>Petstore Table</h4>
				<img src="img/petstore.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
			<div class="container">
				<h4>Pet Table</h4>
				<img src="img/pet.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
