> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application

## Khloe Henry

### Assignment 3 Requirements:

1. Petstore ERD
2. Concert Tickets App
3. Skillset 4 (Decison Structures)
4. Skillset 5 (Nested Structures)
5. Skillset 6 (Methods)

#### README.md file should include the following items:

* Screenshot of ERD
* Screenshot of running Android Studio "Concert Tickets"
* Screenshot of pet table 
* Screeshot of petstore table
* Screenshot of customer table
* Screenshot of skillset 4 (Decision Structures)
* Screenshot of skillset 5 (Nested Structures)
* Screenshot of skillset 6 (Methods)

#### Assignment Links:

* [a3.mwb](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/src/master/a3/kmh19j_a3.mwb "a3.mwb")

* [a3.sql](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a2840b29fc3a80c2ac4ae21c17bf17fc20741626/a3/a3.sql "a3.sql")


#### Assignment Screenshots:


* Screenshot of ERD

![Petstore ERD](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/868213c600191f0358093e3794d0b549b556166d/a3/petstore_erd.png "Petstore ERD")

* Screenshot of running Android Studio "Concert Tickets"

![Concert Tickets A](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/7e2c10f2e9db0f6cde4a2218aa88b0c1aade1b5a/a3/img/Concert_Ticket_A.jpg "Concert Tickets A") ![Concert Tickets B](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/7e2c10f2e9db0f6cde4a2218aa88b0c1aade1b5a/a3/img/Concert_Ticket_B.jpg "Concert Tickets B")

* Screenshot of pet table 

![Pet Table ERD](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/868213c600191f0358093e3794d0b549b556166d/a3/pet.png "Pet Table")

* Screeshot of petstore table

![Petstore Table ERD](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/868213c600191f0358093e3794d0b549b556166d/a3/petstore.png "Petstore Table")

* Screenshot of customer table

![Customer Table ERD](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/868213c600191f0358093e3794d0b549b556166d/a3/customer.png "Customer Table")

* Screenshot of skillset 4 (Decision Structures)

![Skillset 4](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/f30198ee3d4583824bfc16cf077b648b48227881/skillsets/img/DecisionStructures.png "Decison Structures")

* Screenshot of skillset 5 (Nested Structures)

![Skillset 5](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/bf120a66574f8c8414e1087dec5af8cc1cf03f6b/skillsets/img/NestedStructures.png "Nested Structures")

* Screenshot of skillset 6 (Methods)

![Skillset 6](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/1e0ccbf9c15e563197e0d1dbd0759c34bcf81679/skillsets/img/Methods.png "Methods")