<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
	<meta name="author" content="Khloe Henry">
	<link rel="icon" href="favicon.ico">


	<title>LIS4381 - Write/Read File</title>
		<?php include_once("../css/include_css.php"); ?>

		<style>
		body{
			background: #f4e8eb;
		}
		</style>
</head>
<body>

	<?php include_once("../global/nav.php"); ?>

	<div class="container">
		<div class="starter-template">
					<div class="page-header">
						<?php include_once("global/header.php"); ?>	
					</div>

					<h2>File Data</h2>
                    <form action="process.php" role="form" method="post" class="form-horizontal">
                        <div class="form-group">
                            <label for="comment" class="control-label col-sm-2">Comment: </label>
                            <div class="col-sm-10">
                                <textarea name="comment" id="comment" placeholder="Please enter text here..." rows="5" class="form-control"></textarea>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-sm-12">
                                <button type="submit" class="btn btn-default">Submit</button>
                            </div>
                        </div>
                    </form>
                    <?php include_once "global/footer.php"; ?>

                </div>
                </div>


</body>