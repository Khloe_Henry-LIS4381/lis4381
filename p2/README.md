
# LIS4381 Mobile Web Application

## Khloe Henry

### Assignment 5 Requirements:

1. Basic Server-Side Validation
2. Edit and Delete Functions
3. RSS Feed

#### README.md file should include the following items:

* Screenshot of homepage
* Screenshot of Project 2 (index.php)
* Screenshot of Project 2 (edit_petstore.php)
* Screenshot of Project 2 (failed validation)
* Screenshot of Project 2 (passed validation)
* Screenshot of Project 2 (delete record prompt)
* Screenshot of Project 2 (successfully deleted record)
* Screenshot of Project 2 (RSS feed)


#### Assignment Links:

* [Khloe's Online Portfolio](http://localhost:8080/repos/lis4381/index.php "Khloe's Online Portfolio")


#### Assignment Screenshots:

* Screenshot of homepage

![index.php](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/1e133d216d88454859b791dc004610297544a8ff/p2/img/carousel.png "index.php")

* Screenshot of Project 2 (index.php)

![index.php](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/b8e7f047779692616dfe374ad3c74b98405e10a5/p2/img/index.png "index.php") 

* Screenshot of Project 2 (edit_petstore.php)

![edit_petstore.php](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/b8e7f047779692616dfe374ad3c74b98405e10a5/p2/img/edit_petstore.png "edit_petstore.php")

* Screenshot of Project 2 (failed validation)

![failed validation](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/b8e7f047779692616dfe374ad3c74b98405e10a5/p2/img/failed_validation.png "failed validation")

* Screenshot of Project 2 (passed validation)

![passed validation](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/ba8c68168bbcdc7144327189af8039eb84ce2434/p2/img/passed_validation.png "passed validation")

* Screenshot of Project 2 (delete record prompt)

![delete record prompt](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/b8e7f047779692616dfe374ad3c74b98405e10a5/p2/img/delete_record_prompt.png "delete record prompt")

* Screenshot of Project 2 (successfully deleted record)

![successfully deleted record](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/b8e7f047779692616dfe374ad3c74b98405e10a5/p2/img/successfully_deleted_record.png "successfully deleted record")

* Screenshot of Project 2 (RSS feed)

![RSS feed](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/1e133d216d88454859b791dc004610297544a8ff/p2/img/rss_feed.png "RSS feed")
