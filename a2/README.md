> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application

## Khloe Henry

### Assignment 2 Requirements:

1. Healthy Recipes App
2. Skillset 1 (EvenOrOdd)
3. Skillset 2 (Largest Number)
4. Skillset 3 (ArraysAndLoops)

#### README.md file should include the following items:

* Screenshot of running Android Studio "Healthy Recipes"
* Screenshot of skillset 1 (EvenOrOdd)
* Screenshot of skillset 2 (Largest Number)
* Screenshot of skillset 3 (ArraysAndLoops)

#### Assignment Screenshots:

*Screenshot of Android Studio running http://localhost*:

![Healthy Recipes](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/6d9969b642312c9e173ef1937083096586a6bd32/a2/img/app.png "My Recipes Screenshot")

![Healthy Recipes](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/6d9969b642312c9e173ef1937083096586a6bd32/a2/img/app2.png "My Recipes Screenshot")

![EvenOrOdd Skillset](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/496a0c579984fa4c17ed32792d256e2d80275377/skillsets/img/EvenOrOdd.png "Even Or Odd")

![LargestNumber Skillset](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/496a0c579984fa4c17ed32792d256e2d80275377/skillsets/img/LargestNumber.png "LargestNumber")


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")
