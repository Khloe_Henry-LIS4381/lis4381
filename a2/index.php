<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Mark K. Jowett, Ph.D.">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment 2</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style>
			body {
				background-color: #f4e8eb;
			}
		</style>


  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p><strong>Requirements:</strong> 
					Healthy Recipes App; Skillset 1 (EvenOrOdd); Skillset 2 (Largest Number); Skillset 3 (ArraysAndLoops);
				</p>
			<div class="container">
				<h4>Healthy Recipes App</h4>
				<img src="img/app.png" class="img-responsive center-block" style="width: 250px; float: left; margin-left: 300px;" alt="JDK Installation">
				<img src="img/app2.jpg" class="img-responsive center-block" style="width: 241px; float: left; margin-left:20px;" alt="Android Studio Installation">
				</div>
			<div class="container">
				<h4>Even or Odd Java Skillset</h4>
				<img src="img/EvenorOdd.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
			<div class="container">
				<h4>Largest Number Java Skillset</h4>
				<img src="img/LargestNumber.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
