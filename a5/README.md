
# LIS4381 Mobile Web Application

## Khloe Henry

### Assignment 5 Requirements:

1. Basic Server-Side Validation
2. Skillset 13 (Sphere Volume Calculator)
3. Skillset 14 (Simple Calculator)
4. Skillset 15 (Write/Read File)

#### README.md file should include the following items:

* Screenshot of Assignment 5 (index.php)
* Screenshot of Assignment 5 (add_petstore.php invalid)
* Screenshot of Assignment 5 (add_petstore.php failed validation)
* Screenshot of Assignment 5 (add_petstore.php valid)
* Screenshot of Assignment 5 (add_petstore.php passed validation)
* Skillset 13 (Sphere Volume Calculator)
* Skillset 14 (Simple Calculator)
* Skillset 15 (Write/Read File)

#### Assignment Links:

* [Khloe's Online Portfolio](http://localhost:8080/repos/lis4381/index.php "Khloe's Online Portfolio")

* [Simple Calculator](http://localhost:8080/repos/lis4381/simple_calculator/process_functions.php "Simple Calculator")

* [Write/Read File](http://localhost:8080/repos/lis4381/write_read_file/write_read_file.php "Write/Read File")


#### Assignment Screenshots:



* Screenshot of Assignment 5 (index.php)

![index.php](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/dabcfc5ca037bcd100e013e54fc2c6b83b0a7ff9/a5/img/index.png "index.php")

* Screenshots of Assignment 5 (add_petstore.php invalid entry & validation)

![Invalid Entry](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/dabcfc5ca037bcd100e013e54fc2c6b83b0a7ff9/a5/img/invalid.png "Invalid Entry") ![Failed Validation](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/dabcfc5ca037bcd100e013e54fc2c6b83b0a7ff9/a5/img/failed_validation.png "Failed Validation")

* Screenshot of Assignment 5 (add_petstore.php valid entry & validation)

![Valid Entry](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/dabcfc5ca037bcd100e013e54fc2c6b83b0a7ff9/a5/img/valid.png "Valid Entry")![Passed Validation](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/dabcfc5ca037bcd100e013e54fc2c6b83b0a7ff9/a5/img/passed_validation.png "Passed Validation")

* Skillset 13 (Sphere Volume Calculator)

![Skillset 13](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/00e0e59a2caebecc68ef60417e18ed7bc57f356a/a4/img/Screen%20Shot%202021-11-08%20at%2010.20.15%20AM.png "Sphere Volume Calculator")

* Skillset 14 (Simple Calculator)

![Skillset 14A](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/32726ce83e60cc00473bdfcb5be23667471cc3d7/a5/simple_calculator/img/index.png "Simple Calculator A")![Skillset 14B](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/32726ce83e60cc00473bdfcb5be23667471cc3d7/a5/simple_calculator/img/process_functions.png "Simple Calculator B")

![Skillset 14C](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/32726ce83e60cc00473bdfcb5be23667471cc3d7/a5/simple_calculator/img/index2.png "Simple Calculator C")![Skillset 14D](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/32726ce83e60cc00473bdfcb5be23667471cc3d7/a5/simple_calculator/img/process_functions2.png "Simple Calculator D")

* Skillset 15 (Write/Read File)

![Skillset 15A](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/45befa4eeac22212d993328601a873a33c57d3af/write_read_file/img/index.png "Write/Read File")![Skillset 15B](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/45befa4eeac22212d993328601a873a33c57d3af/write_read_file/img/process.png "Write/Read File")