
# LIS4381 Mobile Web Application

## Khloe Henry

### Assignment 4 Requirements:

1. Online Portfolio
2. Basic Client-Side Validation
3. Skillset 10 (Alpha Numeric Special)
4. Skillset 11 (Temperature Conversion)
5. Skillset 11 (Sphere Volume Calculator)

#### README.md file should include the following items:

* Screenshot of Khloe's Online Portfolio
* Screenshot of Client-Side Validation Wrong
* Screenshot of Client-Side Validation Right
* Skillset 10 (Alpha Numeric Special)
* Skillset 11 (Temperature Conversion)
* Skillset 11 (Sphere Volume Calculator)

#### Assignment Links:

* [Khloe's Online Portfolio](http://localhost:8080/repos/lis4381/index.php "Khloe's Online Portfolio")


#### Assignment Screenshots:


* Screenshot of Khloe's Online Portfolio

![Porfolio Home](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/bcddb54c7a84a912004ad9116e606a6435dfe9fc/a4/img/portfolio.png "Portfolio Home")

* Screenshot of Client-Side Validation Wrong

![Petstore Form A](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/bcddb54c7a84a912004ad9116e606a6435dfe9fc/a4/img/portfolio_dv1.png "Petstore Form A") 

* Screenshot of Client-Side Validation Right

![Petstore Form B](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/10a6bb6eefd57916692f561cf5907098bac3a816/a4/img/portfolio_dv2.png "Petstore Form B")

* Screenshot of skillset 10 (Decision Structures)

![Skillset 10](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/00e0e59a2caebecc68ef60417e18ed7bc57f356a/a4/img/Screen%20Shot%202021-11-08%20at%209.54.32%20AM.png "Array List")

* Screenshot of skillset 11 (Nested Structures)

![Skillset 11](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/f1e2db8cf56ccd87be4b5543c6b61df7b7cdaf06/a4/img/skillset11.png "Alpha Numeric Special")

* Screenshot of skillset 12 (Methods)

![Skillset 12](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/00e0e59a2caebecc68ef60417e18ed7bc57f356a/a4/img/Screen%20Shot%202021-11-08%20at%2010.20.15%20AM.png "Temperature Conversion")