import java.util.Scanner;

public class NestedStructures {

    public static void getRequirements()
    {
        System.out.println("Developer: Khloe Henry");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters W or w, C or c, H or h, N or n");
        System.out.println("Use following decision structures: if..else, and switch");

        System.out.println();
    }

    public static void main(String[] args) {
        int nums[] = {3, 2, 4, 99, -1, -5, 3, 7};
        Scanner sc = new Scanner(System.in);
        int search;

        System.out.print("Array length: " + nums.length);

        System.out.print("\nEnter search value: ");
        search = sc.nextInt();

        for (int i = 0; i < nums.length; i++) {
            if(nums[i] == search)
            {
                System.out.println(search + " found at index " + i);
            }
            else {
                System.out.println(search + " *not* found at index " +i);
            }
        }


    }

}