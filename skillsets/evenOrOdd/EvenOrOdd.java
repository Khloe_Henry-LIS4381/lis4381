import java.util.Scanner;

public class EvenOrOdd {

    public static void getRequirements() {
        System.out.println("Developer: Khloe Henry");
        System.out.println("Program evluates integers as even or odd.");
        System.out.println("Note: Program does not check for non-numeric integers");
    }
    public static void main(String[] args) {

        var reader = new Scanner(System.in);

        System.out.print("Enter a number: ");
        int num = reader.nextInt();

        if(num % 2 == 0)
            System.out.println(num + " is even");
        else
            System.out.println(num + " is odd");
    }
}