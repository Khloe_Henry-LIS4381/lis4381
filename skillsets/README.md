# LIS4381 Mobile Web Application Development

## Khloe Henry

### LIS4381 Requirements 

*Skill Set Images:*

* Screenshot of Array Numeric Special

![Alpha Numeric Special](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a0d6b642d12ffb725f38fbc882fbe619a64e6b84/skillsets/img/alphaNumericSpecial.png "Alpha Numeric Special")


* Screenshot of Nested Structures

![Nested Structures](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a0d6b642d12ffb725f38fbc882fbe619a64e6b84/skillsets/img/NestedStructures.png "Nested Structures")

* Screenshot of Decision Structures

![Decision Structures](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a0d6b642d12ffb725f38fbc882fbe619a64e6b84/skillsets/img/DecisionStructures.jpg "Decision Structures")

* Screenshot of Even or Odd

![Even or Odd](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a0d6b642d12ffb725f38fbc882fbe619a64e6b84/skillsets/img/EvenOrOdd.png "Even or Odd")

* Screenshot of Largest Number

![Largest Number](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a0d6b642d12ffb725f38fbc882fbe619a64e6b84/skillsets/img/LargestNumber.png "Largest Number")

* Screenshot of Methods

![Methods](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/a0d6b642d12ffb725f38fbc882fbe619a64e6b84/skillsets/img/Methods.png "Methods")