import java.util.Scanner;

public class Methods {
    
    public static void getRequirements()
    {
        System.out.println("Developer: Khloe Henry");
        System.out.println("Program evaluates user-entered characters.");
        System.out.println("Use following characters W or w, C or c, H or h, N or n");
        System.out.println("Use following decision structures: if..else, and switch");

        System.out.println();
    }

    public static void main(String[] args) {
        String firstName="";
        int userAge = 0;
        String myStr="";
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter first name: ");
        firstName=sc.next();

        System.out.print("Enter age:");
        userAge = sc.nextInt();

        System.out.println();

        System.out.print("void method call: ");
        myVoidMethod(firstName, userAge);

        System.out.print("value-returning method call: ");
        myStr = myValueReturningMethod(firstName, userAge);
        System.out.println(myStr);
       }

public static void myVoidMethod(String first, int age) {
        System.out.println(first + " is " + age);
}

public static String myValueReturningMethod(String first, int age) {
    return first + " is " + age;
}

}