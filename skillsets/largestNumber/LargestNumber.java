import java.util.Scanner;


public class LargestNumber {

    public static void getRequirements() {
        System.out.println("Developer: Khloe Henry");
        System.out.println("Program evaluates the largest of two integers");
        System.out.println("Note: Program does not check for non-numeric character or non-integer values");
    }

	private static Scanner sc;
	public static void main(String[] args) 
	{
		int number1, number2;
		sc = new Scanner(System.in);
		
		System.out.print(" Enter first integer : ");
		number1 = sc.nextInt();	
		
		System.out.print(" Enter second integer : ");
		number2 = sc.nextInt();	
		
		if(number1 > number2) 
	    {
			System.out.println("\n The Largest Number = " + number1);          
	    } 
	    else if (number2 > number1)
	    { 
	    	System.out.println("\n The Largest Number = " + number2);        
	    } 
	    else 
	    {
	    	System.out.println("\n Both are Equal");
	    }		
	}	
}


