<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Khloe Henry">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Project 1</title>		
		<?php include_once("../css/include_css.php"); ?>

		<style>
			body {
				background-color: #f4e8eb;
			}
		</style>


  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-center">
					<strong>Requirements:</strong> Business Card; Concert Tickets App; Random Array Data Validation; Largest of Three Numbers; Array Runtime Data Validation;
					</p>
			<div class="container">
				<h4>Business Card</h4>
				<img src="img/business_card1.jpg" class="img-responsive center-block" style="width: 250px; float: left; margin-left: 300px;" alt="JDK Installation">
				<img src="img/business_card2.jpg" class="img-responsive center-block" style="width: 250px; float: left; margin-left:20px;" alt="Android Studio Installation">
				</div>
			<div class="container">
				<h4>Larget of Three Number Java Skillset</h4>
				<img src="img/LargestOfThreeNumbers.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
			<div class="container">
				<h4>Random Array Java Skillset</h4>
				<img src="img/randomArray.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
			<div class="container">
				<h4>Runtime Array</h4>
				<img src="img/runTimeArray.png" class="img-responsive center-block" style="width:700px; margin-bottom:50px;" alt="AMPPS Installation">
				</div>
		
				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
