> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application

## Khloe Henry

### Assignment 3 Requirements:

1. Business Card
2. Concert Tickets App
3. Skillset 7 (Random Array Data Validation)
4. Skillset 8 (Largest of Three Numbers)
5. Skillset 9 (Array Runtime Data Validation)
6. Extra Credt (Rain Detector)

#### README.md file should include the following items:

* Screenshot of running Android Studio "My Business Card"
* Screenshot of skillset 7 (Random Array Data Validation)
* Screenshot of skillset 8 (Largest of Three Numbers)
* Screenshot of skillset 9 (Rain Detector)


#### Assignment Screenshots:


* Screenshot of running Android Studio "My Business Card"

![Business Card A](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/6bf98df87e74fefa311a3c715e38e1e5bee6e8b6/p1/img/business_card1.jpg "Business Card A") ![Business Card B](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/6bf98df87e74fefa311a3c715e38e1e5bee6e8b6/p1/img/business_card2.jpg "Business Card B")

* Screenshot of skillset 7 (Random Array Data Validation)

![Skillset 7](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/de0237c045e3425733f4af357b5c5cd18c264b5e/p1/img/randomArray.png "Random Array")

* Screenshot of skillset 8 (Largest of Three Numbers)

![Skillset 5](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/de0237c045e3425733f4af357b5c5cd18c264b5e/p1/img/LargestOfThreeNumbers.png "Largest of Three Numbers")

* Screenshot of skillset 9 (Array Runtime Data Validation)

![Skillset 6](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/74e57b4a38fa0ff91be16796c9e24d4c9d22a075/p1/img/runTimeArray.png "Array Runtime")

* Screenshot of extra credit (Rain Detector)

![Skillset 6](https://bitbucket.org/Khloe_Henry-LIS4381/lis4381/raw/1e0ccbf9c15e563197e0d1dbd0759c34bcf81679/skillsets/img/Methods.png "Methods")