> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4381 Mobile Web Application Development

## Khloe Henry

### LIS4381 Requirements 

*Course Work Links:*

1. [A1 README.md](a1/README.md "My a1 README.md file")
    * Install AMPPS
    * Install JDK
    * Install Android Studio and create My First App
    * Provide screenshots of installations 
    * Create Bitbucket repo
    * Complete Bitbucket tutorials (bitbucketstationlocations and myteamquotes)
    * Provide GIT COMMAND DESCRIPTIONS
2. [A2 README.md](a2/README.md "My a2 README.md file")
    * Create Healthy Recipes Android App
    * Provide screenshots of completed app
3. [A3 README.md](a3/README.md "My a3 README.md file")
    * Create ERD based upon business rules
    * Provide screenshot of completed ERD 
    * Provide DB resource links
4. [P1 README.md](p1/README.md "My p1 README.md file")
    * Create a launcher icon image and display it in both activities
    * Must add background color to both activties
    * Must add border around image and button
    * Must add text shadow (button)
    * Provide screenshots of app interfaces
4. [A4 README.md](a4/README.md "My a4 README.md file")
    * Online Portfolio
    * Basic Client-Side Validation
    * Skillset 10 (Alpha Numeric Special)
    * Skillset 11 (Temperature Conversion)
    * Skillset 11 (Sphere Volume Calculator)

6. [A5 README.md](a5/README.md "My a5 README.md file")
    * Basic Server-Side Validation
    * Skillset 13 (Sphere Volume Calculator)
    * Skillset 14 (Simple Calculator)
    * Skillset 15 (Write/Read File)
7. [P2 README.md](p2/README.md "My p2 README.md file")
    * Basic Server-Side Validation
    * Edit and Delete Functions
    * RSS Feed

